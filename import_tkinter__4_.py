import tkinter
master = tkinter.Tk()
canvas = tkinter.Canvas(master, bg='black', height=600, width=600)
for z in range(0,601,75):
    canvas.create_line((0,z),(600,z),fill='white')
for p in range(600,1,-75):
    canvas.create_line((p,0),(p,600),fill='white')
for o in range(0,601,150):
    canvas.create_oval((o,0),(o+75,0+75),fill='white')
    canvas.create_oval((o+75,75),(o+75+75,75+75),fill='white')
    canvas.create_oval((o,150),(o+75,150+75),fill='white')
for u in range(0,601,150):
    canvas.create_oval((u,375),(u+75,375+75),fill='teal')
    canvas.create_oval((u+75,450),(u+75+75,450+75),fill='teal')
    canvas.create_oval((u,525),(u+75,525+75),fill='teal')
canvas.pack()
master.mainloop()

import arcade as q
SCREEN_WIDTH = 600
SCREEN_HEIGHT = 600
SCREEN_TITLE = 'SMAIL'

q.open_window(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)

q.set_background_color((57, 0, 49))

q.start_render()

x = 300
y = 300
r = 200
q.draw_circle_filled(x, y, r, (0, 205, 152))

x = 370
y = 350
r = 20
q.draw_circle_filled(x, y, r, (255, 154, 255))

x = 370
y = 360
w = 120
h = 100
s_a = 0
e_a = 180
q.draw_arc_filled(x, y, w, h, q.color.BLACK, s_a, e_a, 4)

x = 230
y = 360
h = 100
w = 120
s_a = 0
e_a = 180
q.draw_arc_filled(x, y, w, h, q.color.BLACK, s_a, e_a, 4)

x = 230
y = 350
r = 20
q.draw_circle_filled(x, y, r, (255, 154, 255))

x = 300
y = 280
w = 120
h = -100
s_a = 0
e_a = 180
q.draw_arc_outline(x, y, w, h, q.color.BLACK, s_a, e_a, 10)
q.finish_render()
q.run()

from PIL import Image, ImageDraw
width = 1000
height = 1000
sky_color = '#75BBFD'
asphalt_color = '#ffffff'
car_color = '#bF311A'
sun_color = '#FFDB00'
parus_color = '#4d4739'

im = Image.new("RGB", (width, height))
drawer = ImageDraw.Draw(im)

drawer. rectangle(((0, 0), (width, height * 0.8)), sky_color)
drawer. rectangle(((0, height * 0.8), (width, height)), asphalt_color)
drawer.ellipse(((0.8 * width, -0.2 * height),
                (1.2 * width, 0.2 * height)), sun_color)
drawer.polygon(((0.4 * width, height * 0.5),
                (0.6 * width, height * 0.5),
                (0.7 * width, height * 0.7),
                (0.3 * width, height * 0.7)),
                parus_color)
drawer.polygon(((0.45 * width, height * 0.3),
                (0.55 * width, height * 0.3),
                (0.65 * width, height * 0.5),
                (0.35 * width, height * 0.5)),
                parus_color)
drawer.polygon(((0.5 * width, height * 0.1),
                (0.6 * width, height * 0.3),
                (0.4 * width, height * 0.3)),
                parus_color)
drawer.polygon(((0.45 * width, height * 0.7),
                (0.55 * width, height * 0.7),
                (0.55 * width, height * 0.9),
                (0.45 * width, height * 0.9)),
                car_color)

im.show()
import random
from tkinter import Tk, Entry, Button, Label
root = Tk()
root.geometry('220x150+800+400')
root.title('Примеры')
root.resizable(False, False)
x = random.randrange(1, 100)
y = random.randrange(1, 100)
z = str(x)+"+"+str(y)
e = Entry(root, width=20)
b = Button(root, text="Преобразовать")
l = Label(root, bg='black', fg='white', width=20, text=z)
lbl = Label(root, bg='black', fg='white', width=20)

def str22(event):
    global x,y
    s = e.get()
    s = int(s)
    if s != x+y:
        lbl['text'] = 'Неправильно'
    if s == x+y:
        lbl['text'] = 'Правильно'
        x = random.randrange(1, 100)
        y = random.randrange(1, 100)
        n=str(x)+"+"+str(y)
        l.config(text=n)

b.bind('<Button-1>', str22)
l.pack()
e.pack()
b.pack()
lbl.pack()
root.mainloop()

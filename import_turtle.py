import turtle
import random
import time
t = turtle
r = random
t.goto(-100, 0)
t.screensize(2000, 2000)
t.setup(800, 800)
t.ht()
s = 'score'
count = 0
t.title('Упражнения на таблицу умножения v2.2.2.2.2.2.2')

while True:
    y = r.randint(1, 100)
    p = r.randint(1, 100)
    y = str(y)
    p = str(p)
    t.write(y+'x'+p, font=('Segoe Script', 50, 'normal'))
    o = t.numinput('Введите ответ', 'введите ответ')
    if o == 999:
        break
    y = int(y)
    p = int(p)
    if o == y*p:
        t.clear()
        count += 1
        t.color('Blue')
        t.write('ПРАВИЛЬНО!', font=('Arial', 40, 'normal'))
        t.bgcolor('green')
        time.sleep(2)
        t.clear()
        t.write(s+':'+str(count), font=('Arial', 40, 'normal'))
        time.sleep(2)
        t.clear()
    else:
        t.clear()
        count -= 1
        t.write('НЕПРАВИЛЬНО!', font=('Arial', 40, 'normal'))
        t.bgcolor('red')
        time.sleep(2)
        t.clear()
        t.write(s+':'+str(count), font=('Arial', 40, 'normal'))
        time.sleep(2)
        t.clear()
        t.write(y*p)
t.mainloop()
#Type '999' to stop

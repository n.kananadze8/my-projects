import turtle
tina = turtle.Turtle()
screen = turtle.Screen()
screen.bgpic('C:\\Users\\Kanich\\Pictures\\space.gif')
screen.title('space')
screen.setup(1000, 700)
m = 0
a = 0
tina.up()
p = ((0.00, 0.00), (40.00, 0.00), (40.00, -30.00), (70.00, -30.00), (70.00, -
                                                                     50.00), (40.00, -50.00), (40.00, -80.00), (10.00, -80.00), (10.00, 0.00))
screen.register_shape('my_shape', p)
tina.shape('my_shape')
tina.color('yellow')
tina.speed(0)


def up_press():
    global m
    m += 1


def down_press():
    global m
    m -= 1


def left_press():
    global a
    a -= 5


def right_press():
    global a
    a += 5


def space_press():
    global m, a
    m = 0
    a = 0


def home():
    tina.home()


def move():
    global a
    tina.rt(a)
    tina.fd(m)
    a /= 1.2
    x, y = tina.pos()
    if abs(x) > 500 or abs(y) > 350:
        tina.goto(0, 0)
        tina.write('ТЫ ДОСТИГ КОМЕДИИ')
        tina.bg('yellow')
    screen.ontimer(move, 30)


screen.listen()
screen.onkey(up_press, 'Up')
screen.onkey(down_press, 'Down')
screen.onkey(left_press, 'Left')
screen.onkey(right_press, 'Right')
screen.onkey(space_press, 'space')
screen.onkey(home, 'w')
move()

screen.exitonclick()
screen.mainloop()
#Press Up,Down,Right,Left
